set -ueo pipefail

for package_is_a_gemseo_plugin in yes no; do
for project_is_open_source in yes no; do
for gemseo_min_version in develop 5; do
	tmpdir=$(mktemp -d)

	# Uncomment for generating the references.
	# tmpdir=testing/$package_is_a_gemseo_plugin-$project_is_open_source-$gemseo_min_version
	# rm -rf $tmpdir
	# mkdir $tmpdir

	copier copy \
		--defaults \
		--data author_name=an \
		--data author_email=ae \
		--data project_name_suffix=pns \
		--data short_description=sd \
		--data package_is_a_gemseo_plugin=$package_is_a_gemseo_plugin \
		--data project_is_open_source=$project_is_open_source \
		--data gemseo_min_version=$gemseo_min_version \
		$(pwd) \
		$tmpdir

	diff --recursive \
		--ignore-matching-lines=_commit \
		 $(pwd)/testing/$package_is_a_gemseo_plugin-$project_is_open_source-$gemseo_min_version \
		 $tmpdir
done
done
done
